# Boing!

**Boing!** is a simple Bukkit plugin, designed to provide a server with easy-to-use jump pads. Jump pads
are created simply - by placing a stone pressure plate on another block. The type of block under the pressure
plate will determine the power of the jump pad.

Note that this plugin **will not prevent fall damage**. That's up to you to design into your maps. What's soaring
through the air without a little danger, right?

Have you considered slime blocks?

If you prefer, this plugin is also available [on the SpigotMC site](https://www.spigotmc.org/resources/boing.70620/).

## Block power

The plugin aims for the following rough jump heights. Place a stone pressure plate above the indicated block type, 
and walk on it to activate.

* **Coal**: 2-3 blocks
* **Redstone**: 4-6 blocks
* **Lapis**: 7-10 blocks
* **Gold**: 10-14 blocks
* **Iron**: 12-17 blocks
* **Emerald**: 14-20 blocks
* **Diamond**: 16-23 blocks

---

## Project Status

The first version of this project has been released, and should work as expected. Check out the
[releases page](https://gitlab.com/gserv.me/boing/-/releases) to download a copy! Just drop the JAR into your 
`plugins/` folder, restart your server, and you're good to go!

Please note that as this plugin is written in Kotlin, you'll need 
[KotlinPlugin](https://www.spigotmc.org/resources/kotlinplugin-allow-to-use-kotlin-corountines-in-your-plugins.70526/) 
installed to use it.

## Building the project

This project makes use of **Gradle**. Open a terminal and run the following commands from the project directory:

* **Windows**: `gradlew jar`
* **Linux/Mac**: `./gradlew jar`

You will require a JDK installed for this - version 8 or later.

Once this task has been run, you will find a JAR file in `build/lib`, named `Boing-<version>.jar`. This is the
plugin - drop it in your `plugins/` folder to get started.

## Questions & Contributions

Questions, concerns, ideas? [Open an issue!](https://gitlab.com/gserv.me/boing/issues/new) Pull requests are
welcome, once the project has gotten off the ground.
