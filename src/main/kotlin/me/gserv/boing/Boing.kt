package me.gserv.boing
import org.bukkit.plugin.java.JavaPlugin

class Boing : JavaPlugin() {
    override fun onEnable() {
        super.onEnable()
        this.server.pluginManager.registerEvents(PlayerInteractListener(), this)
    }
}