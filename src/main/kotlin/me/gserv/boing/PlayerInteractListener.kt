package me.gserv.boing

import org.bukkit.Material
import org.bukkit.Particle
import org.bukkit.block.BlockFace
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.util.Vector


class PlayerInteractListener : Listener {
    @EventHandler fun onPlayerInteract(event: PlayerInteractEvent) {
        if (event.action == Action.PHYSICAL && event.clickedBlock?.type == Material.STONE_PRESSURE_PLATE) {
            val velocity = Vector()
            val playerVelocity = event.player.velocity

            when (event.clickedBlock?.getRelative(BlockFace.DOWN)?.type) {
                Material.COAL_BLOCK     -> velocity.y = 0.8
                Material.REDSTONE_BLOCK -> velocity.y = 1.1
                Material.LAPIS_BLOCK    -> velocity.y = 1.4
                Material.GOLD_BLOCK     -> velocity.y = 1.7
                Material.IRON_BLOCK     -> velocity.y = 1.9
                Material.EMERALD_BLOCK  -> velocity.y = 2.05
                Material.DIAMOND_BLOCK  -> velocity.y = 2.3

                else                    -> return
            }

            playerVelocity.y = 0.05

            event.player.world.spawnParticle(
                    Particle.SPELL_INSTANT, event.player.location, 200,
                    0.3, 0.3, 0.3
            )

            event.player.velocity = playerVelocity.multiply(1.5).add(velocity)
        }
    }
}

/*

 */